using System;
using System.Net;
using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;
using CSharpKasa;

namespace cskasa
{
    class Options
    {
        // usage examples
        [Usage(ApplicationAlias = "CSharpKasa Sample")]
        public static IEnumerable<Example> Examples
        {
            get
            {
                return new List<Example>()
                 {
                     new Example("discover (default is 255.255.255.255)", new DiscoverOptions { Target = "-t 192.168.0.255" }),
                     new Example("alias", new AliasOptions { IP = "192.168.0.1" }),
                     new Example("sysinfo", new SysInfoOptions { IP = "192.168.0.1" })
                 };
            }
        }
    }

    

    // Discover verb TODO handle actual broadcast subnets
    [Verb("discover", isDefault: true, HelpText = "Discover TP Link devices on the network.")]
    public class DiscoverOptions
    {
        [Option('t', "target", Required = false, HelpText = "The broadcast address to be used for discovery.")]
        public string Target { get; set; }

        public int Run()
        {
            var d = new Discover();
            var devices = new List<SmartDevice>();

            Console.WriteLine("Discovering...");
            try
            {
                if (Target == null)
                {
                    devices = d.All();
                    foreach (var dev in devices)
                    {
                        Console.WriteLine(dev.DumpSystemInfo());
                    }
                }
                else
                {
                    devices.Add(d.Single(Target));
                    Console.WriteLine(devices[0].DumpSystemInfo());
                }

                Console.WriteLine($"{devices.Count} devices found.");
                return 0;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error");
                return 1;
            }
        }
    }

    // Dump Discover verb
    [Verb("dump-discover", HelpText = "Dump discovery information. Useful for dumping into a file to be added to the test suite.")]
    public class DumpDiscoverOptions
    {
        [Option('i', "ip", Default = null, Required = false, HelpText = "Device IP address for single device, 192.168.0.1.")]
        public string IP { get; set; }

        [Option('f', "file", Default = false, Required = false, HelpText = "Dump to file named after alias.")]
        public bool ToFile { get; set; }

        public int Run()
        {
            var d = new Discover();
            Console.WriteLine("Dumping discover...");
            try
            {
                var result = d.Dump(IP, ToFile);
                Console.WriteLine(result);
                return 0;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error");
                return 1;
            }
        }
    }

    [Verb("alias", HelpText = "Get or set the device (or plug) alias.")]
    public class AliasOptions
    {
        // [Option('i', "ip", Default = null, Required = false, HelpText = "The device IP address, 192.168.0.1.")]
        [Value(0, Default = null, Required = false, HelpText = "The device IP address: 192.168.0.1.")]
        public string IP { get; set; }

        // [Option('n', "name", Default = null, Required = false, HelpText = "The new alias.")]
        [Value(1, Default = null, Required = false, HelpText = "The new alias: no_spaces_string")]
        public string Name { get; set; }

        public int Run()
        {
            try
            {
                // print alias
                if (IP != null && Name == null)
                {
                    Console.WriteLine("Getting alias...");
                    var sp = new SmartPlug(IP);
                    Console.WriteLine($"{sp.State.Alias}");
                }
                // set the alias to Name
                else if (IP != null && Name != null)
                {
                    Console.WriteLine("Setting alias...");
                    var sp = new SmartPlug(IP);
                    sp.Set(SmartDevice.SetOptions.Alias, Name);
                    Console.Write(sp.State.Alias);
                }
                // print all aliases
                else
                {
                    Console.WriteLine("Getting all aliases...");
                    var d = new Discover();
                    var devices = d.All();
                    foreach (var dev in devices)
                    {
                        Console.WriteLine(dev.State.Alias);
                    }
                }
                return 0;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error");
                return 1;
            }
        }
    }

    [Verb("on", HelpText = "Turn the device(s) on. (Omit IP address to turn on all devices).")]
    public class OnOptions
    {
        [Value(0, Default = null, Required = false, HelpText = "The device IP address, 192.168.0.1.")]
        public string IP { get; set; }

        public int Run()
        {
            try
            {
                var sp = new SmartPlug();
                var msg = "Failed";
                var result = false;

                if (IP != null)
                {
                    Console.Write($"Turning On {IP}... ");
                    result = sp.TurnOn(IP);
                    if (result)
                        msg = "Success";
                    Console.WriteLine(msg);
                    return 0;
                }
                else
                {
                    Console.WriteLine($"Turning On all devices... ");
                    var plugs = SmartPlug.TurnOnAll();
                    foreach (var plug in plugs)
                    {
                        var rs = plug.State.RelayState == 1 ? "on" : "off";
                        Console.WriteLine($"{plug.State.Alias} is {rs}");
                    }
                    return 0;
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return 1;
            }
        }
    }

    [Verb("off", HelpText = "Turn the device(s) off. (Omit IP address to turn off all devices).")]
    public class OffOptions
    {
        [Value(0, Default = null, Required = false, HelpText = "The device IP address, 192.168.0.1.")]
        public string IP { get; set; }
        public int Run()
        {
            try
            {
                var sp = new SmartPlug();
                var msg = "Failed";
                var result = false;

                if (IP != null)
                {
                    Console.Write($"Turning Off {IP}... ");
                    result = sp.TurnOff(IP);
                    if (result)
                        msg = "Success";
                    Console.WriteLine(msg);
                    return 0;
                }
                else
                {
                    Console.WriteLine($"Turning Off all devices... ");
                    var plugs = SmartPlug.TurnOffAll();
                    foreach (var plug in plugs)
                    {
                        var rs = plug.State.RelayState == 1 ? "on" : "off";
                        Console.WriteLine($"{plug.State.Alias} is {rs}");
                    }
                    return 0;
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                return 1;
            }
        }
    }

    [Verb("reboot", HelpText = "Reboot the device.")]
    public class RebootOptions
    {

        [Option('i', "ip", Required = false, HelpText = "The device IP address, 192.168.0.1.")]
        public string IP { get; set; }

        public int Run()
        {
            var sp = new SmartPlug();
            Console.Write("Rebooting... ");
            try
            {
                sp.Reboot(IP);
                Console.WriteLine("Done");
                return 0;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error");
                return 1;
            }
        }
    }

    // Sysinfo verb
    [Verb("sysinfo", HelpText = "Print out full system information.")]
    public class SysInfoOptions
    {
        [Option('i', "ip", Required = false, HelpText = "The device IP address, 192.168.0.1.")]
        public string IP { get; set; }

        public int Run()
        {
            var sd = new SmartPlug();
            Console.WriteLine("Get System Info... ");
            try
            {
                Console.WriteLine(sd.DumpSystemInfo());
                return 0;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Error");
                return 1;
            }
        }
    }

    // LED verb
    [Verb("led", HelpText = "Get or set (Plug's) led state.")]
    public class LedOptions
    {
        [Value(0, Required = true, HelpText = "The device IP address, 192.168.0.1.")]
        public string IP { get; set; }

        [Value(1, Default = null, Required = false, HelpText = "Either ON or OFF.")]
        public string Switch { get; set; }

        public int Run()
        {
            try
            {
                var sp = new SmartPlug();
                sp.Host = IPAddress.Parse(IP);
                string msg = "Failed";
                
                // turn LED on or off
                if (Switch != null)
                {
                    if (Switch.ToLower() == "on")
                    {
                        Console.Write("Set LED On...");
                        msg = sp.SetLEDOn() == true ? "Success" : "Failed";
                    }
                    else if (Switch.ToLower() == "off")
                    {
                        Console.Write("Set LED Off...");
                        msg = sp.SetLEDOff() == true ? "Success" : "Failed";
                    }
                    else
                    {
                        msg = "on/off not supplied.";
                    }
                }
                // get LED state
                else
                {
                    Console.Write("Get LED state...");
                    sp.Update();
                    msg = sp.State.LedOff == 1 ? "Off" : "On";
                }

                Console.WriteLine(msg);
                return 0;
            }
            catch (System.Exception)
            {
                return 1;
            }
        }
    }
}
