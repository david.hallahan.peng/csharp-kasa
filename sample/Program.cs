﻿using System;
using Microsoft.Extensions.Logging;
using CommandLine;

namespace cskasa
{
    class Program
    {
     static int Main(string[] args)
        {

            // handle Options verbs
            return CommandLine.Parser.Default.ParseArguments<Options, DiscoverOptions, AliasOptions, OnOptions, OffOptions, RebootOptions, SysInfoOptions, LedOptions>(args)
                .MapResult(
                (DiscoverOptions opts) => opts.Run(),
                (AliasOptions opts) => opts.Run(),
                (OnOptions opts) => opts.Run(),
                (OffOptions opts) => opts.Run(),
                (RebootOptions opts) => opts.Run(),
                (SysInfoOptions opts) => opts.Run(),
                (LedOptions opts) => opts.Run(),
                 errs => 1);

        }
    }
}
