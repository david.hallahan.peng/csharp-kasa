# CSharpKasa

Port of the [python-kasa](https://github.com/python-kasa/python-kasa) project. 

## Usage
- The [src](./src) directory has the classlib for the Nuget package.
- The [sample](./sample) directory has a console app which uses the Nuget package.
- The Nuget package is not yet published.
  
```bash
cskasa         # will run a Discovery of all devices
cskasa --help  # for help
```

## Disclaimers
- Only handles TPLink [SmartPlugs](https://www.kasasmart.com/us/products/smart-plugs) (the only devices I had to test with).
