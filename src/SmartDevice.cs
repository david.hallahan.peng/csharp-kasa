using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace CSharpKasa
{

    public class SmartDevice
    {
        public IPAddress Host { get; set; }
        public SmartDeviceState State { get; set; }

        // key, command
        private static readonly Hashtable SetCommandLookup = new Hashtable(){
            {"alias", "set_dev_alias"},
            {"macaddr", "set_mac_addr"},
        };

        // key used for lookup
        public enum SetOptions
        {
            Alias,
            MacAddr
        }

        public SmartDevice()
        {

        }

        // constructor loaded with device state from host ip address
        public SmartDevice(string host)
        {
            Host = IPAddress.Parse(host);
            Update();
        }

        // instance populated from json, host
        public SmartDevice(string json, IPAddress host)
        {
            Host = host;
            this.State = SmartDeviceState.Deserialize(json);
        }

        // update instance with status from device
        public void Update()
        {
            GetSystemInfo();
        }
        
        // Populates State with device info
        public void GetSystemInfo(IPAddress host = null)
        {
            if (host != null)
                Host = host;
            if (Host == null)
                throw new ArgumentNullException(Host.ToString(), "Ensure Host is provided or has been set.");
            
            var p = new Protocol();
            try
            {
                Task<string> result = Task.Run(() => p.QueryAsync(Discover.DISCOVERY_QUERY, Host.ToString()));
                result.Wait();
                State = SmartDeviceState.Deserialize(result.Result);
            }
            catch (FormatException)
            {
                throw new FormatException("The host is malformed: " + Host.ToString());
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException(Host.ToString(), "Set the Host first.");
            }
            catch (ArgumentException)
            {
                throw new ArgumentException(Host.ToString(), "The Host is likely malformed.");
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public string DumpSystemInfo()
        {
            var s = new StringBuilder();
            s.Append($"=== {State.Alias.ToUpper()} {Host.ToString()}\n");
            s.Append($"DeviceID: {State.DeviceId} \n");
            s.Append($"sw_ver: {State.SwVer} \n");
            s.Append($"hw_ver: {State.HwVer} \n");
            s.Append($"model: {State.Model} \n");
            s.Append($"oemId: {State.OemId} \n");
            s.Append($"hwId: {State.HwId} \n");
            s.Append($"rssi: {State.Rssi} \n");
            s.Append($"longitude_i: {State.LongitudeI} \n");
            s.Append($"latitude_i: {State.LatitudeI} \n");
            s.Append($"Alias: {State.Alias} \n");
            s.Append($"status: {State.Status} \n");
            s.Append($"mic_type: {State.MicType} \n");
            s.Append($"feature: {State.Feature} \n");
            s.Append($"mac: {State.Mac} \n");
            s.Append($"updating: {State.Updating} \n");
            s.Append($"led_off: {State.LedOff} \n");
            s.Append($"relay_state: {State.RelayState} \n");
            s.Append($"on_time: {State.OnTime} \n");
            s.Append($"active_mode: {State.ActiveMode} \n");
            s.Append($"icon_hash: {State.IconHash} \n");
            s.Append($"dev_name: {State.DevName} \n");
            s.Append($"next_action: type: {State.NextAction.Type} \n");
            s.Append($"err_code: {State.ErrCode} \n");
            s.Append($"\n");
            return s.ToString();
        }

        // Set properties (or send commands) to device
        public bool Set(SetOptions option, string value, IPAddress host = null)
        {
            // sample: "{\"system\":{\"set_dev_alias\":{\"alias\":"new alias name"}}}";
            if (host != null)
                Host = host;
            if (Host == null)
                throw new ArgumentNullException("Ensure Host is provided or has been set.");
            try
            {
                var command = (string)SetCommandLookup[option.ToString().ToLower()];

                JObject commandJo = new JObject(
                    new JProperty("system",
                        new JObject(
                            new JProperty(command,
                                new JObject(
                                    new JProperty(option.ToString().ToLower(), value))))));
                var json = commandJo.ToString();
                
                var p = new Protocol();
                var dev = Host.ToString();
                Task<string> resultJson = Task.Run(() => p.QueryAsync(json, dev));
                resultJson.Wait();
                Update();
                
                if (State.Alias == value)
                    return true;    

                return false;
            }
            catch (System.Exception)
            {
                throw;
            }

        }

        /// Serialize State to disk (optionally) but return JSON of SmartDevice list
        static public string Serialize(List<SmartDevice> smartDevices, bool toFile = false)
        {
            var states = new List<SmartDeviceState>();
            foreach (var sd in smartDevices)
            {
                var json = SmartDeviceState.Serialize(sd.State, toFile);
                states.Add(sd.State);
            }

            var result = JsonConvert.SerializeObject(states);
            return result;
        }
    }


}

