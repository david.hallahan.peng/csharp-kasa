using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace CSharpKasa
{
    public class Protocol
    {
        public const int TIMEOUT = 3000; //msec

        // messages received from clients
        public List<(string json, IPAddress address)> Devices
        {
            get { return _devices; }
        }
        private readonly ILogger _logger;
        public const int TPLINKPORT = 9999;
        private const int ANYPORT = 0; // choose any available port
        private int _CurrentPort = 0; // use any available, unless overridden
        private const int TPLINKINITVECTOR = 171;
        private List<string> _messages = new List<string>();
        private List<(string json, IPAddress ip)> _devices = new List<(string, IPAddress)>();
        private Socket _udpSocket;
        private byte[] _buffer = new byte[1024];

        public Protocol()
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });

            ILogger logger = loggerFactory.CreateLogger<Protocol>();
            _logger = logger;
        }

        public void StartReceiving(int timeout)
        {
            try
            {
                _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                var localIpe = new IPEndPoint(IPAddress.Any, _CurrentPort);
                _udpSocket.Bind(localIpe);

                // _logger.LogInformation("Receiving on port " + ((IPEndPoint)_udpSocket.LocalEndPoint).Port);

                var ipe = new IPEndPoint(IPAddress.Any, 0);
                var remoteEp = (EndPoint)ipe;

                do
                {
                    Task<SocketReceiveFromResult> srfr = SocketTaskExtensions.ReceiveFromAsync(_udpSocket, _buffer, SocketFlags.None, remoteEp);
                    ProcessMessage(srfr.Result.ReceivedBytes, srfr.Result.RemoteEndPoint);

                } while (_udpSocket.Available > 0);

                if (_udpSocket != null)
                {
                    _udpSocket.Close();
                    _udpSocket.Dispose();
                }
            }
            catch (System.Exception e)
            {
                _logger.LogError($"StartReceiving: {e.Message}");
                throw;
            }
        }

        // process message from device
        private void ProcessMessage(int length, EndPoint ep)
        {
            var message = new byte[length];
            Array.Copy(_buffer, message, length);
            var json = Decrypt(message);
            _messages.Add(json);
            var ipe = (IPEndPoint)ep;
            (string, IPAddress) d = (json, ipe.Address);
            _devices.Add(d);
        }

        // send UDP broadcast looking for devices
        public void Broadcast(string message)
        {
            const string broadcastIp = "255.255.255.255";
            try
            {
                IPEndPoint broadcastIpe = new IPEndPoint(IPAddress.Parse(broadcastIp), TPLINKPORT);

                // choose any port 
                var udpClient = new UdpClient(ANYPORT);

                // set to current for use by Receive or others
                _CurrentPort = ((IPEndPoint)(udpClient.Client.LocalEndPoint)).Port;

                var datagram = Encrypt(message);
                var sent = udpClient.Send(datagram, datagram.Length, broadcastIpe);
                udpClient.Close();
                // _logger.LogInformation($"Broadcast {sent.ToString()} message: {message}");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
        }

        // send query to device
        public async Task<string> QueryAsync(string requestJson, string host, int retryCount = 5)
        {
            try
            {
                var json = requestJson;

                // encrypt
                var data = this.Encrypt(json.ToString());

                // add header
                data = AddHeader(data);

                // write command
                var tcpClient = new TcpClient(host, TPLINKPORT);
                tcpClient.Client.SendTimeout = Protocol.TIMEOUT;
                NetworkStream stream = tcpClient.GetStream();
                await Task.Run(() => stream.Write(data, 0, data.Length));

                // read response
                var buffer = new Byte[tcpClient.ReceiveBufferSize];
                Int32 streamLength = await Task.Run(() => stream.Read(buffer, 0, buffer.Length));
                tcpClient.Close();
                stream.Close();

                // get message from stream
                byte[] trimBuffer = new byte[streamLength];
                Array.Copy(buffer, trimBuffer, streamLength);
                var noHeader = TrimHeader(trimBuffer); 
                var responseJson = Decrypt(noHeader);

                return responseJson;
            }
            catch (SocketException e)
            {
                _logger.LogError(e.Message);
                _logger.LogError("Socket Error Code: " + e.SocketErrorCode);
            }
            catch (System.Exception e)
            {
                _logger.LogError("Query: " + e.Message);
            }
            return null;
        }

        public byte[] Encrypt(string request)
        {
            // encode
            IEnumerable<byte> plainbytes = Encoding.ASCII.GetBytes(request);

            // key for cipher
            var key = TPLINKINITVECTOR;

            // encrypt
            var buffer = new List<byte>();
            byte cipherbyte;
            foreach (var plainbyte in plainbytes)
            {
                cipherbyte = (byte)(key ^ plainbyte); //xor
                key = cipherbyte;
                buffer.Add((byte)cipherbyte);
            }

            return buffer.ToArray();
        }

        public string Decrypt(byte[] ciphertext)
        {
            var key = TPLINKINITVECTOR;
            var buffer = new List<byte>();

            byte plainbyte;
            foreach (var cipherbyte in ciphertext)
            {
                plainbyte = (byte)(key ^ cipherbyte); //xor
                key = cipherbyte;
                buffer.Add(plainbyte);
            }

            byte[] plaintext = buffer.ToArray<byte>();
            var decode = Encoding.ASCII.GetString(plaintext);
            return decode;
        }

        // TPLink requires a header of 4 bytes containing the length of the data
        byte[] AddHeader(byte[] bytes)
        {
            byte[] length = BitConverter.GetBytes(bytes.Count()); // returns 4 byte array
            var lengthRev = length.Reverse();
            IEnumerable<byte> headerAndBytes = lengthRev.Concat(bytes);
            return headerAndBytes.ToArray<byte>();
        }

        // remove 4 byte length header
        public byte[] TrimHeader(byte[] bytes)
        {
            // remove header
            const int HEADERLENGTH = 4;
            if (bytes.Length > 0)
            {
                byte[] bytesTrim = new byte[bytes.Length - HEADERLENGTH];
                Array.Copy(bytes, HEADERLENGTH, bytesTrim, 0, bytesTrim.Length);
                return bytesTrim;
            }
            return bytes;
        }
    }
}
