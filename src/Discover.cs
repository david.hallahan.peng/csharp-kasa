using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace CSharpKasa
{
    public class Discover
    {
        public const string DISCOVERY_QUERY = "{\"system\":{\"get_sysinfo\":null}}";
        // public const string DISCOVERY_QUERY = "{\"system\": {\"get_sysinfo\": null}, \"emeter\": {\"get_realtime\": null}, \"smartlife.iot.dimmer\": {\"get_dimmer_parameters\": null}, \"smartlife.iot.common.emeter\": {\"get_realtime\": null}, \"smartlife.iot.smartbulb.lightingservice\": {\"get_light_state\": null}}";

        private readonly ILogger _logger;
        const int DISCOVERY_TIMEOUT = 3000;

        public Discover()
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });

            ILogger logger = loggerFactory.CreateLogger<Protocol>();
            _logger = logger;
        }

        // Returns list of all devices on local network
        public List<SmartDevice> All()
        {
            var smartDevices = new List<SmartDevice>();
            try
            {
                var p = new Protocol();
                p.Broadcast(Discover.DISCOVERY_QUERY);

                // receive broadcast responses
                p.StartReceiving(DISCOVERY_TIMEOUT / 1000);

                foreach (var dev in p.Devices)
                {
                    var sd = new SmartDevice(dev.json, dev.address);
                    smartDevices.Add(sd);
                }
                return smartDevices;
            }
            catch (System.Exception e)
            {
                _logger.LogInformation($"All: {e.Message}");
                throw;
            }
        }

        // Returns device at IP address (host)
        public SmartDevice Single(string host)
        {
            try
            {
                return new SmartDevice(host);
            }
            catch (System.Exception e)
            {
                _logger.LogInformation($"Single: {e.Message}");
                throw;
            }

        }

        // Dumps device info, optionally writing to file. 
        public string Dump(string host = null, bool toFile = false)
        {
            try
            {
                if (host == null)
                {
                    var sds = All();
                    var json = SmartDevice.Serialize(sds, toFile);
                    return json;
                }
                else
                {   
                    var sds = new List<SmartDevice>();
                    sds.Add(Single(host));
                    var json = SmartDevice.Serialize(sds, toFile);
                    return json;
                }
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}
