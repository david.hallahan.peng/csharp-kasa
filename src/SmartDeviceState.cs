using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CSharpKasa
{
    // stores the state of the device
    public class SmartDeviceState
    {
        [JsonProperty("sw_ver")]
        public string SwVer { get; set; }

        [JsonProperty("hw_ver")]
        public string HwVer { get; set; }

        [JsonProperty("model")]
        public string Model { get; set; }

        [JsonProperty("deviceId")]
        public string DeviceId { get; set; }

        [JsonProperty("oemId")]
        public string OemId { get; set; }

        [JsonProperty("hwId")]
        public string HwId { get; set; }

        [JsonProperty("rssi")]
        public long Rssi { get; set; }

        [JsonProperty("longitude_i")]
        public long LongitudeI { get; set; }

        [JsonProperty("latitude_i")]
        public long LatitudeI { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("mic_type")]
        public string MicType { get; set; }

        [JsonProperty("feature")]
        public string Feature { get; set; }

        [JsonProperty("mac")]
        public string Mac { get; set; }

        [JsonProperty("updating")]
        public long Updating { get; set; }

        [JsonProperty("led_off")]
        public long LedOff { get; set; }

        [JsonProperty("relay_state")]
        public long RelayState { get; set; }

        [JsonProperty("on_time")]
        public long OnTime { get; set; }

        [JsonProperty("active_mode")]
        public string ActiveMode { get; set; }

        [JsonProperty("icon_hash")]
        public string IconHash { get; set; }

        [JsonProperty("dev_name")]
        public string DevName { get; set; }

        [JsonProperty("next_action")]
        public NextAction NextAction { get; set; }

        [JsonProperty("err_code")]
        public long ErrCode { get; set; }

        // Sample data
        //  "sw_ver": "1.1.3 Build 200804 Rel.095135",
        //	"hw_ver": "2.1",
        //	"model": "HS103(US)",
        //	"deviceId": "800647BBD57AE7E7DCA308D4EFE7D3411D793BFD",
        //	"oemId": "24B27E003E98D053CF8B51CE8086FDA8",
        //	"hwId": "18967AEED1C89BED1BBC7F62FD06468A",
        //	"rssi": -46,
        //	"longitude_i": 0,
        //	"latitude_i": 0,
        //	"alias": "fireplaceRight",
        //	"status": "new",
        //	"mic_type": "IOT.SMARTPLUGSWITCH",
        //	"feature": "TIM",
        //	"mac": "60:32:B1:49:FD:84",
        //	"updating": 0,
        //	"led_off": 1,
        //	"relay_state": 0,
        //	"on_time": 0,
        //	"active_mode": "none",
        //	"icon_hash": "",
        //	"dev_name": "Smart Wi-Fi Plug Lite",
        //	"next_action": {
        //		"type": -1
        //	},
        //	"err_code": 0

        // Deserializes system info
        static public SmartDeviceState Deserialize(string json)
        {
            try
            {
                JObject jo = JObject.Parse(json);
                JToken jt = jo["system"]["get_sysinfo"];
                var smartDeviceState = jt.ToObject<SmartDeviceState>();
                return smartDeviceState;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        // Serialize SmartDeviceState to JSON, optionally to file named for alias
        static public string Serialize(SmartDeviceState smartDeviceState, bool toFile = false)
        {
            try
            {
                var json = JsonConvert.SerializeObject(smartDeviceState);

                if (toFile)
                {
                    using (StreamWriter file = File.CreateText($"{smartDeviceState.Alias}.json"))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, smartDeviceState);
                    }
                }
                return json;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

    }

    public partial class NextAction
    {
        [JsonProperty("type")]
        public long Type { get; set; }
    }

}

