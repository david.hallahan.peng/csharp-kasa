using System;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSharpKasa
{
    public class SmartPlug : SmartDevice
    {

        public SmartPlug()
        {

        }
        public SmartPlug(string host) : base(host)
        {

        }

        public bool TurnOn(string host = null)
        {
            if (host != null)
                Host = IPAddress.Parse(host);
            var p = new Protocol();
            const string cmd = "{\"system\":{\"set_relay_state\":{\"state\":1}}}";
            Task<string> result = Task<string>.Run(() => p.QueryAsync(cmd, Host.ToString()));
            result.Wait();
            Update();
            return State.ErrCode == 0 ? true : false;
        }
        static public List<SmartPlug> TurnOnAll()
        {
            var d = new Discover();
            var sds = d.All();
            var result = new List<SmartPlug>();
            foreach (var sd in sds)
            {
                var sp = new SmartPlug(sd.Host.ToString());
                sp.TurnOn();
                result.Add(sp);
            }
            return result;
        }
        public bool TurnOff(string host = null)
        {
            if (host != null)
                Host = IPAddress.Parse(host);
            var p = new Protocol();
            const string cmd = "{\"system\":{\"set_relay_state\":{\"state\":0}}}";
            Task<string> result = Task<string>.Run(() => p.QueryAsync(cmd, Host.ToString()));
            result.Wait();
            Update();
            return State.ErrCode == 0 ? true : false;
        }

        static public List<SmartPlug> TurnOffAll()
        {
            var d = new Discover();
            var sds = d.All();
            var result = new List<SmartPlug>();
            foreach (var sd in sds)
            {
                var sp = new SmartPlug(sd.Host.ToString());
                sp.TurnOff();
                result.Add(sp);
            }
            return result;
        }

        public bool SetLEDOff(string host = null)
        {
            if (host != null)
                Host = IPAddress.Parse(host);
            var p = new Protocol();
            string cmd = "{\"system\":{\"set_led_off\":{\"off\":1}}}";
            Task<string> result = Task<string>.Run(() => p.QueryAsync(cmd, Host.ToString()));
            result.Wait();
            Update();
            return State.ErrCode == 0 ? true : false;
        }

        public bool SetLEDOn(string host = null)
        {
            if (host != null)
                Host = IPAddress.Parse(host);
            var p = new Protocol();
            string cmd = "{\"system\":{\"set_led_off\":{\"off\":0}}}";
            Task<string> result = Task<string>.Run(() => p.QueryAsync(cmd, Host.ToString()));
            result.Wait();
            Update();
            return State.ErrCode == 0 ? true : false;
        }
        public bool Reboot(string host = null)
        {
            if (host != null)
                Host = IPAddress.Parse(host);
            var p = new Protocol();
            string cmd = "{\"system\":{\"reboot\":{\"delay\":2}}";
            Task<string> result = Task<string>.Run(() => p.QueryAsync(cmd, Host.ToString()));
            result.Wait();
            return true; // assume it works, does not respond
        }
    }
}